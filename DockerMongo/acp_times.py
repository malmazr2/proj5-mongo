"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    # Getting the time from user input ans setting the time zone to Pacific time
    theTime = arrow.get(brevet_start_time).replace(tzinfo='US/Pacific')
    # The total time we will add to the time after calculation
    totalTime = 0

    # This to handle numbers above the 20% of the brevet distnation
    # It is kind of error handling by outputting for the user the beginning time as an error
    if control_dist_km > brevet_dist_km and control_dist_km > (brevet_dist_km + (brevet_dist_km * 0.2)):

        control_dist_km = 0

    # This to handle number in 20% range of the brevet distnation
    # It always return the times of the brevet distnation
    elif control_dist_km > brevet_dist_km and control_dist_km <= (brevet_dist_km + (brevet_dist_km * 0.2)):

        control_dist_km = brevet_dist_km

    # This handles any checkpoint in the brevet 
    if control_dist_km <= brevet_dist_km:

        # The 600 and 1000 range calculation
        if control_dist_km > 600 and control_dist_km <=1000:

            controlTime = (control_dist_km - 600)/ 28
            totalTime += controlTime
            control_dist_km -= control_dist_km - 600

        # The 400 and 600 range calculation
        if control_dist_km > 400 and control_dist_km <=600:
                
            controlTime = (control_dist_km - 400)/ 30
            totalTime += controlTime
            control_dist_km -= control_dist_km - 400

        # the 200 and 400 range calculation    
        if control_dist_km > 200 and control_dist_km <=400:
        
            controlTime = (control_dist_km - 200)/ 32
            totalTime += controlTime
            control_dist_km -= control_dist_km - 200

        # the 0 and 200 range calculation
        if control_dist_km > 0 and control_dist_km <= 200:
            
            controlTime = (control_dist_km - 0)/ 34 
            totalTime += controlTime

        # the 0 checkpoint calculation
        if control_dist_km == 0:

            totalTime += 0

    # The hours to be added
    hours = int(totalTime)
    # The minutes to be added
    minutes = round(60 * (totalTime - hours))
    # Adding the hours and minutes in the user selected time
    theTime = theTime.shift(hours =+ hours, minutes =+ minutes)
        
    # returning the new opening time
    return theTime.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    # Getting the time from user input ans setting the time zone to Pacific time
    theTime = arrow.get(brevet_start_time).replace(tzinfo='US/Pacific')
    # The total time we will add to the time after calculation
    totalTime = 0

    # This to handle numbers above the 20% of the brevet distnation                                                                       
    # It is kind of error handling by outputting for the user the beginning time as an error
    if control_dist_km > brevet_dist_km and control_dist_km > (brevet_dist_km + (brevet_dist_km * 0.2)):

        control_dist_km = 0
        totalTime = -1 # This is because i made a calculation below and total time was affected so i made it = -1

    # This to handle number in 20% range of the brevet distnation                                                                         
    # It always return the times of the brevet distnation   
    elif control_dist_km > brevet_dist_km and control_dist_km <= (brevet_dist_km + (brevet_dist_km * 0.2)):
        
        control_dist_km = brevet_dist_km

    # This to handle the checkpoint that equels the brevet dist
    # for example 200,200 should return the 200 closing time following the
    # fixed ACP brevet times.
    if control_dist_km == brevet_dist_km:
        
        if brevet_dist_km == 1000:
            totalTime = 75.00
            
        elif brevet_dist_km == 600:
            totalTime = 40.00

        elif brevet_dist_km == 400:
            totalTime = 27.00

        elif brevet_dist_km == 300:
            totalTime = 20.00

        elif brevet_dist_km == 200:
            totalTime = 13.50

    # This handles any checkpoint in the brevet
    elif control_dist_km < brevet_dist_km:

        # The 600 and 1000 range calculation
        if control_dist_km > 600 and control_dist_km <=1000:

            controlTime = (control_dist_km - 600)/ 11.428
            totalTime += controlTime
            control_dist_km -= control_dist_km - 600

        # The 400 and 600 range calculation
        if control_dist_km > 400 and control_dist_km <=600:

            controlTime = (control_dist_km - 400)/ 15
            totalTime += controlTime
            control_dist_km -= control_dist_km - 400

        # The 200 and 400 range calculation
        if control_dist_km > 200 and control_dist_km <=400:

            controlTime = (control_dist_km - 200)/ 15
            totalTime += controlTime
            control_dist_km -= control_dist_km - 200

        # The 0 and 200 range calculation
        if control_dist_km >= 0 and control_dist_km <= 200:

            # Calculating the time from the starting point to 60 km
            # Added 1 hour to the starting point closing time as it is fixed
            if control_dist_km >= 0 and control_dist_km <=60:
                controlTime = (control_dist_km)/20
                totalTime += 1 + controlTime
            # Calculating everything else    
            else:    
                controlTime = (control_dist_km - 0)/ 15
                totalTime += controlTime

    # The hours to be added
    hours = int(totalTime)
    # The minutes to be added
    minutes = round(60 * (totalTime - hours))
    # Added to the hours and minutes in the user selected time
    theTime = theTime.shift(hours =+ hours, minutes =+ minutes)

    # Returning the new closing time
    return theTime.isoformat()
