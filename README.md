# Project 5: Brevet time calculator with Ajax and MongoDB
# Majed Almazrouei - malmazr2@uoregon.edu

------------------------------------------------
The project is a simple list of controle times from project 4 stored in MongoDB database.

- Created a submit button and display button
- Storing the data from our calculator to the MongoDB
- Loading the data from our database to a new webpage
- Creating a new page with the opening time and closing time using user input

Error Handling.
- If submit is clicked and there is no input, the page will refresh.
- If Display is clicked and there is no data in the database, the page will refresh and wont open a new webpage.



